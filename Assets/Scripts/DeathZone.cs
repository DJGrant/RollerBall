﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour {

	public Vector3 spawnPoint = new Vector3(0f,1f,0f);
	public float deathGround = -100f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.transform.position.y < deathGround) {
			gameObject.transform.position = spawnPoint;
			if (gameObject.GetComponent<Rigidbody> () != null) {
				gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
				gameObject.GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
			}
		}
	}
}
