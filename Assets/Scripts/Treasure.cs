﻿using UnityEngine;
using System.Collections;

public class Treasure : MonoBehaviour {

	public int value = 10;
	public GameObject explosionPrefab;
	public AudioClip sound;
	public GameObject audioSource;

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "Player") {
			if (GameManager.gm!=null)
			{
				// tell the game manager to Collect
				GameManager.gm.Collect (value);
			}
			
			// explode if specified
			if (explosionPrefab != null) {
				Instantiate (explosionPrefab, transform.position, Quaternion.identity);
			}

			if (sound != null && audioSource != null) {
				StaticStuff.InstantiateSound (gameObject,  sound, audioSource);
			}
			
			// destroy after collection
			Destroy (gameObject);
		}
	}
}
