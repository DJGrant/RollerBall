﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperScript : MonoBehaviour {

	Animator animator;
	public AudioClip hitSound;
	public GameObject audioSource;

	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Player") {
			animator.SetTrigger ("IsHit");
			if (hitSound != null && audioSource != null) {
				StaticStuff.InstantiateSound (gameObject, hitSound, audioSource);
			}
		}
	}
}
