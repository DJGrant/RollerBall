﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterScript : MonoBehaviour {

	public GameObject target;
	bool isTurnedOn = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player" && target != null) {
			if (target.GetComponent<TeleporterScript> () != null) {
				if (isTurnedOn) {
					col.gameObject.transform.position = target.transform.position;
					target.GetComponent<TeleporterScript> ().isTurnedOn = false;
				}
			} else {
				col.gameObject.transform.position = target.transform.position;
			}
		}
	}

	void OnTriggerExit(Collider col) {
		if (col.gameObject.tag == "Player")
			isTurnedOn = true;
	}
}
