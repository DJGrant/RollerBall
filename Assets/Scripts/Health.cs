﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour {
	
	public enum deathAction {loadLevelWhenDead,doNothingWhenDead};
	
	public float healthPoints = 1f;
	public float respawnHealthPoints = 1f;		//base health points
	
	public int numberOfLives = 1;					//lives and variables for respawning
	public bool isAlive = true;	

	public GameObject explosionPrefab;
	
	public deathAction onLivesGone = deathAction.doNothingWhenDead;
	
	public string LevelToLoad = "";

	public AudioClip damageSound;
	public AudioClip deathSound;
	public GameObject soundInstance;
	
	private Vector3 respawnPosition;
	private Quaternion respawnRotation;
	

	// Use this for initialization
	void Start () 
	{
		// store initial position as respawn location
		respawnPosition = transform.position;
		respawnRotation = transform.rotation;
		
		if (LevelToLoad=="") // default to current scene 
		{
			//LevelToLoad = Application.loadedLevelName;
			LevelToLoad = SceneManager.GetActiveScene().name;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (healthPoints <= 0) {				// if the object is 'dead'
			numberOfLives--;					// decrement # of lives, update lives GUI

			if (gameObject.tag == "Player" && GameManager.gm!=null)
			{
				GameManager.gm.LivesAnim ();
				gameObject.GetComponent<Animator> ().SetTrigger ("Respawn");
				if (gameObject.GetComponent<TrailRenderer> () != null)
					gameObject.GetComponent<TrailRenderer> ().Clear ();
			}

			
			if (explosionPrefab!=null) {
				Instantiate (explosionPrefab, transform.position, Quaternion.identity);
			}
			
			if (numberOfLives > 0) { // respawn
				transform.position = respawnPosition;	// reset the player to respawn position
				transform.rotation = respawnRotation;
				healthPoints = respawnHealthPoints;	// give the player full health again

				if (deathSound != null && soundInstance != null)
					StaticStuff.InstantiateSound (gameObject,  deathSound, soundInstance);

				if (gameObject.GetComponent<Rigidbody> () != null) {
					gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
					gameObject.GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
				}

			} else { // here is where you do stuff once ALL lives are gone)
				isAlive = false;

				healthPoints = 0;

				if (deathSound != null && soundInstance != null)
					StaticStuff.InstantiateSound (gameObject,  deathSound, soundInstance);
				
				switch(onLivesGone)
				{
				case deathAction.loadLevelWhenDead:
					//Application.LoadLevel (LevelToLoad);
					SceneManager.LoadScene (LevelToLoad, LoadSceneMode.Single);
					break;
				case deathAction.doNothingWhenDead:
					// do nothing, death must be handled in another way elsewhere
					break;
				}
				Destroy(gameObject);
			}
		}
	}
	
	public void ApplyDamage(float amount)
	{	
		healthPoints = healthPoints - amount;

		if (damageSound != null && soundInstance != null)
			StaticStuff.InstantiateSound (gameObject,  damageSound, soundInstance);

		if (gameObject.tag == "Player" && gameObject.GetComponent<Animator> () != null)
			gameObject.GetComponent<Animator> ().SetTrigger ("IsDamaged");

		if (gameObject.tag == "Player" && GameManager.gm!=null)
		{
			GameManager.gm.HealthAnim ();
		}
	}
	
	public void ApplyHeal(float amount)
	{
		healthPoints = healthPoints + amount;
		if (gameObject.tag == "Player" && GameManager.gm!=null)
		{
			GameManager.gm.HealthAnim ();
		}
	}

	public void ApplyBonusLife(int amount)
	{
		numberOfLives = numberOfLives + amount;

		if (gameObject.tag == "Player" && GameManager.gm!=null)
		{
			GameManager.gm.LivesAnim ();
		}
	}
	
	public void updateRespawn(Vector3 newRespawnPosition, Quaternion newRespawnRotation) {
		respawnPosition = newRespawnPosition;
		respawnRotation = newRespawnRotation;
	}
}
