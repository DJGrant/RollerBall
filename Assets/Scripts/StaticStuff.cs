﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticStuff : MonoBehaviour {

	// Use this to instantiate a sound
	public static void InstantiateSound (GameObject ob, AudioClip sound, GameObject audioSource) {
		GameObject soundInstance;
		soundInstance = Instantiate (audioSource, ob.transform.position, Quaternion.identity);
		soundInstance.GetComponent<AudioSource> ().clip = sound;
		soundInstance.GetComponent<TimedObjectDestructor> ().timeOut = sound.length;
		soundInstance.GetComponent<AudioSource> ().Play();
	}
}
